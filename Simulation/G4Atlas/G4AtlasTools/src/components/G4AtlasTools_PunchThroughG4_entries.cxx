#include "../PunchThroughG4Tool.h"
#include "../PunchThroughG4Classifier.h"
#include "../PunchThroughSimWrapper.h"

DECLARE_COMPONENT( PunchThroughG4Tool )
DECLARE_COMPONENT( PunchThroughG4Classifier )
DECLARE_COMPONENT( PunchThroughSimWrapper )
