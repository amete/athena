/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ITKPIXEL_ENCODINGALG_H
#define ITKPIXEL_ENCODINGALG_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "InDetRawData/PixelRDO_Container.h"
#include "StoreGate/ReadHandleKey.h"
#include "ITkPixelHitSortingTool.h"
#include "ITkPixelEncodingTool.h"
#include "ITkPixelDataPackingTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/WriteHandleKey.h"

namespace InDetDD {
  class PixelDetectorManager;
}

class PixelID;


class ITkPixelEncodingAlg : public AthReentrantAlgorithm 
{
  public:

    ITkPixelEncodingAlg(const std::string &name, ISvcLocator *pSvcLocator);

    virtual StatusCode initialize() override;
    virtual StatusCode execute(const EventContext& ctx) const override;

  private:

    typedef std::vector< std::vector<uint32_t >> ITkPacketCollection;
  
    SG::ReadHandleKey<PixelRDO_Container> m_pixelRDOKey{this, "PixelRDOKey", "ITkPixelRDOs", "StoreGate Key of Pixel RDOs"};
    SG::WriteHandleKey<std::vector<uint32_t>> m_EncodedStreamKey{this, "EncodedStreamKey", "ITkEncodedStream", "StoreGate Key for Encoded Stream"};


    static constexpr float s_pitch50x50=0.050;

    ToolHandle<ITkPixelHitSortingTool> m_hitSortingTool;
    ToolHandle<ITkPixelEncodingTool> m_encodingTool;
    ToolHandle<ITkPixelDataPackingTool> m_packingTool;


};
#endif

